from werkzeug.exceptions import Unauthorized
import requests
import json
import flask
from flask import Flask, request, Response, make_response
from flask import send_from_directory
from flask import send_file
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import urllib.parse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property


# config
app = flask.Flask(__name__)
app.config["PROPAGATE_EXCEPTIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
api = Api(app, version='1.0', title='FUCK-CORS API')

CORS(app, origins=r'*', allow_headers=r'*')

# parser for fuck-cors
parser = api.parser()

parser.add_argument(
    "url", type=str, required=True, help="URL")

# parser for fuck-cors
parser_str = api.parser()

parser_str.add_argument(
    "json", type=str, required=True, help="JSON-String")


# parser for fuck-cors-JSON
parser_jsn = api.parser()

parser_jsn.add_argument(
    "json", type=str, required=False, help="json object")



# parser for proxy
parser_proxy = api.parser()

parser_proxy.add_argument(
    "jsn_object", type=object, required=True, help="JSON Object")


# parser for proxy_jsn_url
parser_proxy_url = api.parser()

parser_proxy_url.add_argument(
    "url", type=str, required=True, help="url with json string")



jsn_example = fields.String(example= 
{
    "url": "url to bypass cors",
    "headers": {
        "cookies": "your cookies hiere",
        "accept": "format",
        "accept-encoding": "encoding"
    },
    "payload":{
        "jwt":"your jwt token",
        "something":"else"
    },
}
)

# Route for Fuck-Cors
@api.route('/fuck-cors', doc={"description": "proxy request with string"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser)
    def get(self):
        args = parser.parse_args()

        return fuck_cors(args)

# Fuck_Cors
def fuck_cors(args):

    url = args['url']

    resp = requests.get(f'{url}')
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
    response = Response(resp.content, resp.status_code, headers)
    return response


# Route for Fuck-Cors
@api.route('/fuck-cors-json', doc={"description": "proxy request with json object"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_jsn)
    def post(self):

        args = request.get_json()

        return fuck_cors_json(args)

# Fuck_Cors
def fuck_cors_json(args):

    url = args['url']

    headers = args['headers']
    payload = args['payload']
    resp = requests.get(f'{url}', headers=headers)
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
    response = Response(resp.content, resp.status_code, headers)
    return response

# Route for Fuck-Cors-str
@api.route('/fuck-cors-str', doc={"description": "proxy with json string"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_str)
    def get(self):

        args = parser_str.parse_args()

        return fuck_cors_json_str(args)

# Fuck_Cors
def fuck_cors_json_str(args):

    args = json.loads(args['json'])
    chat = args['url']
    img_id = args['id']

    if chat == "Diez":
        url = "https://mod-panel.bettsport.net/api/v1/message/media/{}".format(img_id)
    elif chat == "Ares":
        url = "https://mod-panel.anonyme-flirts.net/api/v1/message/media/{}".format(img_id)
    else:
        url = chat

    headers = args['headers']
    resp = requests.get(f'{url}', headers=headers)
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection', 'cookie']
    headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
    response = Response(resp.content, resp.status_code, headers)
    return response


# Route for Fuck-Cors-str
@api.route('/proxy', doc={"description": "proxy request"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_proxy)
    def post(self):

        args = request.get_json()

        return make_proxy(args)

# Fuck_Cors
def make_proxy(args):

    url = args['destination']
    #url = "http://127.0.0.1:5002/manes-get-message"

    payload = json.dumps(args)
    headers = {
    'Content-Type': 'application/json'  
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    jsn = json.loads(response.text)
    return jsn




# Route for Fuck-Cors-str
@api.route('/proxy_url', doc={"description": "proxy request expect json in url"})
class getMessageares(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_proxy_url)
    def get(self):

        args = parser_proxy_url.parse_args()

        return make_proxy_url(args)

# Fuck_Cors
def make_proxy_url(args):

    url = urllib.parse.unquote(args['url'])
    #url = "http://127.0.0.1:5002/manes-get-message"
    
    resp = requests.request("GET", url)
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection', 'cookie']
    headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
    response = Response(resp.content, resp.status_code, headers)
    return response



@ api.errorhandler(ValueError)
def handle_value_exception(error):
    err_message = str(error)
    return {'message': err_message}, 400

if __name__ == "__main__":
    app.run()
